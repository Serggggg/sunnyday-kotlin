package com.anadea.svv.kotlin.sunnyday.data.model.errors

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorMessage(
        @SerializedName("cod") @Expose val errCode : Int,
        @SerializedName("message") @Expose val message : String)