package com.anadea.svv.kotlin.sunnyday.data

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.anadea.svv.kotlin.sunnyday.App
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.helpers.LocationHelper
import com.anadea.svv.kotlin.sunnyday.data.helpers.PreferencesHelper
import com.anadea.svv.kotlin.sunnyday.data.model.errors.ErrorMessage
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoAdapter
import com.anadea.svv.kotlin.sunnyday.data.remote.RestService
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import retrofit2.HttpException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


class DataManager {

    companion object {
        private const val TAG = "DataManager"
    }

    @Inject
    lateinit var context: Context

    @Inject
    protected lateinit var restService: RestService

    @Inject
    protected lateinit var preferencesHelper: PreferencesHelper

    @Inject
    protected lateinit var locationHelper: LocationHelper

    val internetIsAvailable: Boolean
        get() = (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
                .activeNetworkInfo?.isConnected == true

    // Places and locations from Preferences
    private val userLocationsMap = LinkedHashMap<String, LocationInfo>()
    // User's current GPS location
    var userGpsLocationInfo: LocationInfo? = null
        private set

    // Stored User locations publisher
    private val userLocationsBehaviorSubject = BehaviorSubject.create<List<LocationInfo>>()
    // Current weather info publisher
    private val currentWeatherSubject = BehaviorSubject.create<WeatherInfo>()
    // Forecast info publisher
    private val forecastBehaviorSubject = BehaviorSubject.create<Map<String, List<WeatherInfo>>>()

    init {
        App.getAppComponent().inject(this)
    }

    fun getStoredLocationInfoByName(locationName: String): LocationInfo? = userLocationsMap[locationName]

    fun isLocationInFavorites(locationName: String): Boolean = userLocationsMap.contains(locationName)

    /**
     * Subscribing a new consumer to stored user's locations info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    fun subscribeUserLocationsConsumer(consumer: Consumer<List<LocationInfo>>): Disposable =
            userLocationsBehaviorSubject
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(consumer)

    /**
     * Subscribing a new consumer to current weather info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    fun subscribeCurrentWeatherConsumer(consumer: Consumer<WeatherInfo>,
                                        throwableConsumer: Consumer<Throwable>): Disposable =
            currentWeatherSubject
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(consumer, throwableConsumer)

    /**
     * Subscribing a new consumer to forecast info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    fun subscribeForecastPage(consumer: Consumer<Map<String, List<WeatherInfo>>>,
                              throwableConsumer: Consumer<Throwable>): Disposable =
            forecastBehaviorSubject
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(consumer, throwableConsumer)

    /**
     * Requesting and publishing user's places and locations from SharedPreferences
     * into userLocationsBehaviorSubject
     */
    fun requestAndPublishStoredUserLocations() {
        preferencesHelper.readStoredUserLocations()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe { locationInfoList ->
                    userLocationsMap.clear()
                    locationInfoList.forEach { userLocationsMap.put(it.cityName, it) }
                    userLocationsBehaviorSubject.onNext(locationInfoList)
                }
    }

    /**
     * Add new location info to stored user locations and save list to SharedPreferences
     *
     * @param locationInfo: new Location info
     */
    fun addLocationToFavorites(locationInfo: LocationInfo): Completable {

        val storedLocations: List<LocationInfo> = userLocationsMap
                .mapTo(ArrayList()) { it.value }
                .apply { this.add(locationInfo) }

        return preferencesHelper.saveStoredUserLocations(storedLocations)
                .doOnComplete {
                    userLocationsMap.put(locationInfo.cityName, locationInfo)
                    userLocationsBehaviorSubject.onNext(storedLocations)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Remove locations from stored user locations and save list to SharedPreferences
     *
     * @param removeList: List of locations to remove
     */
    fun removeLocationsFromFavorites(removeList: List<LocationInfo>): Completable {

        val storedLocations = (userLocationsMap - removeList.map { it.cityName })
                .map { it.value }

        return preferencesHelper.saveStoredUserLocations(storedLocations)
                .doOnComplete { userLocationsBehaviorSubject.onNext(storedLocations) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Requesting stored weather info for last known user's GPS location from SharedPreferences
     *
     * @return Single source with WeatherInfoAdapter (includes location name)
     */
    fun requestLastStoredWeatherInfo(): Single<WeatherInfoAdapter> =
            preferencesHelper.readLastWeatherInfoFromPrefs()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    /**
     * Saving weather info for last known user's GPS location to SharedPreferences
     *
     * @param weatherInfoAdapter: WeatherInfoAdapter (includes location name)
     */
    fun saveWeatherInfo(weatherInfoAdapter: WeatherInfoAdapter) =
            preferencesHelper.saveLastWeatherInfoToPrefs(weatherInfoAdapter)

    /**
     * Requesting current user's GPS location
     *
     * @return Single source with LocationInfo received via GooglePlaces API
     */
    fun requestCurrentUserLocation(): Single<LocationInfo> {

        return locationHelper.requestCurrentPlace(context)
                .observeOn(Schedulers.io())
                .doOnSuccess { locationInfo ->
                    if (!locationInfo.isEmpty)
                        userGpsLocationInfo = locationInfo
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Requesting and publishing current weather into currentWeatherSubject
     *
     * @param latitude:  location's latitude
     * @param longitude: location's longitude
     */
    fun requestAndPublishCurrentWeather(latitude: Double, longitude: Double) {

        restService.requestCurrentWeather(latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { weatherInfo, throwable ->
                    if (throwable == null)
                        currentWeatherSubject.onNext(weatherInfo)
                    else
                        currentWeatherSubject.onError(throwable)
                }
    }

    /**
     * Requesting and publishing 5 day forecast includes weather data every 3 hours info
     * into forecastBehaviorSubject
     *
     * @param latitude:  location latitude
     * @param longitude: location longitude
     */
    fun requestAndPublishFiveDaysForecast(latitude: Double, longitude: Double) {

        restService.requestFiveDaysForecast(latitude, longitude)
                .map { forecastInfo ->
                    // Transform forecast WeatherInfo list into LinkedHashMap
                    // (KEY = date, VALUE = List of WeatherInfo)
                    val dateFormatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                    forecastInfo.weatherInfoList.groupBy { dateFormatter.format(Date(it.timestamp * 1000)) }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe { forecastMap, throwable ->
                    if (throwable == null)
                        forecastBehaviorSubject.onNext(forecastMap)
                    else
                        forecastBehaviorSubject.onError(throwable)
                }
    }

    fun parseErrorMessage(throwable: Throwable): ErrorMessage =
            when (throwable) {
                is HttpException -> {
                    val body = throwable.response().errorBody()
                    try {
                        Gson().fromJson(body?.string(), ErrorMessage::class.java)
                    } catch (e: IOException) {
                        Log.e(TAG, "Error message parse failed: " + e.message)
                    } as ErrorMessage
                }
                is IOException -> {
                    val errorMessage = String.format(Locale.getDefault(), "%s: %s",
                            context.getString(R.string.err_network_error), throwable.message)
                    ErrorMessage(0, errorMessage)
                }
                else -> {
                    val errorMessage = String.format(Locale.getDefault(), "%s: %s",
                            context.getString(R.string.err_unexpected_error), throwable.message)
                    ErrorMessage(0, errorMessage)
                }
            }
}