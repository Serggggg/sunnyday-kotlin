package com.anadea.svv.kotlin.sunnyday.features.customize_dialog

import android.support.v7.widget.RecyclerView
import android.view.View
import java.lang.ref.WeakReference

class LocationListItemViewHolder(itemView: View,
                                 private val itemClickListenerReference: WeakReference<ItemClickListener>) : RecyclerView.ViewHolder(itemView) {

    interface ItemClickListener {
        fun onListItemClick(view: View, position: Int)
    }

    init {
        itemView.setOnClickListener { view ->
            itemClickListenerReference.get()?.onListItemClick(view, adapterPosition) }
    }

}