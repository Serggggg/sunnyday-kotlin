package com.anadea.svv.kotlin.sunnyday.data.remote

import com.anadea.svv.kotlin.sunnyday.data.model.forecast.ForecastInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestService {

    private val TAG = "RestService"

    private val openWeatherApi: OpenWeatherApi

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(OpenWeatherInterceptor())
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        openWeatherApi = retrofit.create(OpenWeatherApi::class.java)
    }

    fun requestCurrentWeather(latitude: Double, longitude: Double) : Single<WeatherInfo> =
            openWeatherApi.getCurrentWeather(latitude, longitude)

    fun requestFiveDaysForecast(latitude: Double, longitude: Double) : Single<ForecastInfo> =
            openWeatherApi.getFiveDaysForecast(latitude, longitude)
}