package com.anadea.svv.kotlin.sunnyday.data.model.location

import java.util.*

data class LocationInfo (
        val cityName : String = "",
        val countryName : String? = null,
        val latitude : Double? = null,
        val longitude : Double? = null) : Comparable<LocationInfo> {

    val isEmpty get() = cityName == "" && countryName == null && latitude == null && longitude == null

    val latLong get() = String.format(Locale.getDefault(), "%.6f, %.6f", latitude, longitude)

    override fun compareTo(other: LocationInfo): Int = this.cityName.compareTo(other.cityName)
}