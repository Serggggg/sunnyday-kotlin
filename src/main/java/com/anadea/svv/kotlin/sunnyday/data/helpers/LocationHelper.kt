package com.anadea.svv.kotlin.sunnyday.data.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.PlaceLikelihoodBuffer
import com.google.android.gms.location.places.Places
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*

class LocationHelper {

    private fun obtainGoogleApiClient(context: Context,
                                      onConnectionFailedListener: GoogleApiClient.OnConnectionFailedListener): GoogleApiClient =
            GoogleApiClient.Builder(context)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addOnConnectionFailedListener(onConnectionFailedListener)
                    .build()

    /**
     * Requesting current GPS location via Google Play Services
     *
     * @param context: app context
     * @return Single source with recognized LocationInfo
     */
    @SuppressLint("MissingPermission")
    fun requestCurrentPlace(context: Context): Single<LocationInfo> =

            Single.create { emitter ->

                val googleApiClient = obtainGoogleApiClient(context,
                        GoogleApiClient.OnConnectionFailedListener { connectionResult -> emitter.onError(Throwable(connectionResult.errorMessage)) })
                googleApiClient.connect()

                // Permissions are checked and requested by Presenter
                val result = Places
                        .PlaceDetectionApi
                        .getCurrentPlace(googleApiClient, null)

                result.setResultCallback { placeLikelihoods ->
                    handleLikelyPlaces(placeLikelihoods,
                            Geocoder(googleApiClient.context, Locale.getDefault()))
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .subscribe({ locationInfo -> emitter.onSuccess(locationInfo) })
                }
            }

    /**
     * Handling likely places received via PlaceDetectionApi.getCurrentPlace()
     * Method selects most likely place and creates a LocationInfo object via Geocoder
     *
     * @param likelyPlaces: PlaceLikelihoodBuffer with likely places
     * @param geocoder: Geocoder instance
     * @return Single source with LocationInfo
     */
    private fun handleLikelyPlaces(likelyPlaces: PlaceLikelihoodBuffer,
                                   geocoder: Geocoder): Single<LocationInfo> {

        return Single.fromCallable {
            // Getting most likely place
            var recognizedLocationInfo = LocationInfo()

            var mostLikelyPlace: Place? = null
            var mostLikelihood = -1f
            for (placeLikelihood in likelyPlaces) {
                if (placeLikelihood.likelihood > mostLikelihood) {
                    mostLikelihood = placeLikelihood.likelihood
                    mostLikelyPlace = placeLikelihood.place
                }
            }

            // Transform most likely place to LocationInfo with GeoCoder
            mostLikelyPlace?.let {
                if (Geocoder.isPresent())
                    try {
                        val addressList = geocoder
                                .getFromLocation(it.latLng.latitude, it.latLng.longitude, 5)
                        if (!addressList.isEmpty()) {
                            val address = addressList[0]
                            recognizedLocationInfo = LocationInfo(address.locality, address.countryName,
                                    address.latitude, address.longitude)
                        }
                    } catch (e: IOException) {
                        throw Exception("GeoCoder error: " + e.message)
                    } finally {
                        likelyPlaces.release()
                    }
            }

            recognizedLocationInfo
        }
    }

}