package com.anadea.svv.kotlin.sunnyday.di.modules

import android.content.Context
import com.anadea.svv.kotlin.sunnyday.data.DataManager
import com.anadea.svv.kotlin.sunnyday.data.helpers.LocationHelper
import com.anadea.svv.kotlin.sunnyday.data.helpers.PreferencesHelper
import com.anadea.svv.kotlin.sunnyday.data.remote.RestService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    internal fun provideDataManager(): DataManager = DataManager()

    @Provides
    internal fun provideLocationHelper(): LocationHelper = LocationHelper()

    @Provides
    internal fun providePreferencesHelper(context: Context): PreferencesHelper =
            PreferencesHelper(context)

    @Provides
    internal fun provideRestService(): RestService = RestService()

}