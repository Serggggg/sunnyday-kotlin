package com.anadea.svv.kotlin.sunnyday.data.model.forecast

import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ForecastInfo (
        @SerializedName("list") @Expose val weatherInfoList : List<WeatherInfo>)