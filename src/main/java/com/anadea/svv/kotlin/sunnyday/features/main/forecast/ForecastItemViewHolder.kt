package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import android.support.v7.widget.RecyclerView
import android.view.View
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.forecast_list_item.view.*

class ForecastItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindWeatherInfoData(weatherInfoModel: WeatherInfoModel) =
            with(itemView) {
                Picasso.with(context)
                        .load(weatherInfoModel.getIconPath())
                        .into(forecast_list_item_icon_imageView)
                forecast_list_item_temp_textView.text = weatherInfoModel.getTemperature()
                forecast_list_item_time_textView.text = weatherInfoModel.getWeatherTime()
                forecast_list_item_description_textView.text = weatherInfoModel.getDescription()
                forecast_list_item_pressure_textView.text = weatherInfoModel.getPressure()
                forecast_list_item_wind_textView.text = weatherInfoModel.getWindSpeed()
                forecast_list_item_humidity_textView.text = weatherInfoModel.getHumidity()
            }
}