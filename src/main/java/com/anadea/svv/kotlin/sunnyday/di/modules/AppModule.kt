package com.anadea.svv.kotlin.sunnyday.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideAppContext(): Context = application.applicationContext
}