package com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter

import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import java.text.SimpleDateFormat
import java.util.*

fun String.capitalizeFirst(): String =
        this.substring(0, 1).toUpperCase() + this.substring(1)

class WeatherInfoAdapter(
        private val locationName: String? = null,
        private val weatherInfo: WeatherInfo? = null) : WeatherInfoModel {

    companion object {
        private val TIME_FORMAT = SimpleDateFormat("HH:mm", Locale.US)
        private val DATE_FORMAT = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        private val DATE_EXT_FORMAT = SimpleDateFormat("EEEE, d MMM yyyy", Locale.getDefault())

        fun transformWeatherInfoList(locationName: String,
                                     weatherInfoList: List<WeatherInfo>): List<WeatherInfoAdapter> =
                weatherInfoList.map { WeatherInfoAdapter(locationName, it) }
    }

    val isEmpty get() = locationName == null && weatherInfo == null

    override fun getLocationName(): String? = locationName

    override fun getTemperature(): String {
        val temp = weatherInfo?.common?.temperature?.toInt() ?: 0
        val plusSign = if (temp > 0) "+" else ""
        return String.format(Locale.getDefault(), "%s%d", plusSign, temp)
    }

    override fun getWeatherDate(): String = DATE_FORMAT.format(Date((weatherInfo?.timestamp ?: 0) * 1000))

    override fun getWeatherDateExt(): String = DATE_EXT_FORMAT.format(Date((weatherInfo?.timestamp ?: 0) * 1000))

    override fun getWeatherTime(): String = TIME_FORMAT.format(Date((weatherInfo?.timestamp ?: 0) * 1000))

    override fun getIconPath(): String {
        return if (weatherInfo?.descriptionList?.isEmpty() != false)
            ""
        else {
            "http://openweathermap.org/img/w/" + weatherInfo.descriptionList[0].icon + ".png"
        }
    }

    override fun getMaxTemperature(): String =
            weatherInfo?.common?.temperatureMax?.toInt().toString()

    override fun getMinTemperature(): String =
            weatherInfo?.common?.temperatureMin?.toInt().toString()

    override fun getHumidity(): String =
            String.format(Locale.US, "%d%%", weatherInfo?.common?.humidity?.toInt())

    override fun getWindSpeed(): String = (weatherInfo?.wind?.speed?.toInt()).toString()

    override fun getPressure(): String = (weatherInfo?.common?.pressure?.toInt()).toString()

    override fun getDescription(): String {
        return if (weatherInfo?.descriptionList?.isEmpty() != false)
            ""
        else
            weatherInfo.descriptionList[0].description?.capitalizeFirst() ?: ""
    }

//    companion object {
//        fun transformWeatherInfoList(locationName: String,
//                                     weatherInfoList: List<WeatherInfo>): List<WeatherInfoAdapter> =
//                weatherInfoList.map { WeatherInfoAdapter(locationName, it) }
//    }

}