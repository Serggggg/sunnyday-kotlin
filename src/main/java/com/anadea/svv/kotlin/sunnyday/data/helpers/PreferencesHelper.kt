package com.anadea.svv.kotlin.sunnyday.data.helpers

import android.content.Context
import android.content.SharedPreferences
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoAdapter
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*

class PreferencesHelper(private val context: Context) {

    companion object {
        private const val PREFERENCES_FILE_NAME = "preferences"
        private const val PREFERENCE_NAME_USER_LOCATIONS = "user_locations"
        private const val PREFERENCE_NAME_LAST_WEATHER_INFO = "last_weather_info"
    }

    private val preferences: SharedPreferences
        get() = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)

    /**
     * Read list of stored (favorites) user's locations from SharedPreferences
     *
     * @return Single source with list of LocationInfo
     */
    fun readStoredUserLocations(): Single<List<LocationInfo>> {

        return Single.fromCallable<List<LocationInfo>> {
            val userLocationsStringSet: Set<String> =
                    preferences.getStringSet(PREFERENCE_NAME_USER_LOCATIONS, LinkedHashSet())
            userLocationsStringSet.map { Gson().fromJson(it, LocationInfo::class.java) }
        }
    }

    /**
     * Saving list of stored (favorites) user's locations to SharedPreferences
     *
     * @param locationInfoList: list of LocationInfo to save
     * @return Completable source
     */
    fun saveStoredUserLocations(locationInfoList: List<LocationInfo>): Completable =
            Completable.fromAction {
                preferences.edit().putStringSet(PREFERENCE_NAME_USER_LOCATIONS, locationInfoList
                        .asSequence()
                        .map { Gson().toJson(it) }
                        .toSet())
                        .apply()
            }

    /**
     * Read stored weather info for last known user's GPS location from SharedPreferences
     *
     * @return Single source with WeatherInfoAdapter (includes location name)
     */
    fun readLastWeatherInfoFromPrefs(): Single<WeatherInfoAdapter> {

        return Single.fromCallable<WeatherInfoAdapter> {

            val lastWeatherInfoPreference: String? = preferences.getString(PREFERENCE_NAME_LAST_WEATHER_INFO, null)
            if (lastWeatherInfoPreference != null)
                Gson().fromJson(lastWeatherInfoPreference, WeatherInfoAdapter::class.java)
            else
                WeatherInfoAdapter()
        }
    }

    /**
     * Saving weather info for last known user's GPS location to SharedPreferences
     */
    fun saveLastWeatherInfoToPrefs(weatherInfoAdapter: WeatherInfoAdapter) {
        preferences.edit().putString(PREFERENCE_NAME_LAST_WEATHER_INFO, Gson().toJson(weatherInfoAdapter)).apply()
    }
}