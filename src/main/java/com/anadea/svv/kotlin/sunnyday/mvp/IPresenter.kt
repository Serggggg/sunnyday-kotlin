package com.anadea.svv.kotlin.sunnyday.mvp

interface IPresenter<in T> {

    fun attachView(mvpView: T)

    fun detachView()

    fun destroy()
}