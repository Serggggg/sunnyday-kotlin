package com.anadea.svv.kotlin.sunnyday.data.model.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Common (
        @SerializedName("temp") @Expose val temperature: Float? = null,
        @SerializedName("pressure") @Expose val pressure: Float? = null,
        @SerializedName("humidity") @Expose val humidity: Float? = null,
        @SerializedName("temp_min") @Expose val temperatureMin: Float? = null,
        @SerializedName("temp_max") @Expose val temperatureMax: Float? = null)