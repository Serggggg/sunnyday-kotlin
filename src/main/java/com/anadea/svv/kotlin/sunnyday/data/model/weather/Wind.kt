package com.anadea.svv.kotlin.sunnyday.data.model.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Wind (
        @SerializedName("speed") @Expose val speed : Float? = null,
        @SerializedName("deg") @Expose val degree: Float? = null)