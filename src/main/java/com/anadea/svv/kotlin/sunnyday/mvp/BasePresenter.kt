package com.anadea.svv.kotlin.sunnyday.mvp

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V : IView> : IPresenter<V> {

    private var mvpView : V? = null

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun attachView(mvpView: V) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        compositeDisposable.clear()
        mvpView = null
    }

    override fun destroy() = Unit

    protected fun getView() : V? = mvpView

    protected fun isViewAttached() : Boolean = mvpView != null
}