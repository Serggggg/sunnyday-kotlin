package com.anadea.svv.kotlin.sunnyday.features.main

import android.app.Activity
import android.view.MenuItem
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoModel
import com.anadea.svv.kotlin.sunnyday.mvp.IPresenter
import com.anadea.svv.kotlin.sunnyday.mvp.IView

interface MvpMainContract {

    interface View : IView {

        fun onError(message: String)

        fun startProgress()

        fun stopProgress()

        fun checkLocationPermissions()

        fun updateNavigationDrawerMenu(userLocationsList: List<LocationInfo>)

        fun updateNavHeaderLocationData(locationInfo: LocationInfo)

        fun updateCurrentWeatherData(weatherInfoModel: WeatherInfoModel)

        fun setAddToFavButtonVisible(enableAddToFav: Boolean)

        fun updateForecastDates(forecastDates: List<String>)

        fun showLocationsCustomizeDialog()
    }

    interface Presenter : IPresenter<View> {

        fun onLocationPermissionsGranted()

        fun onRefreshRequested()

        fun onSearchButtonClick(activity: Activity, requestCode: Int)

        fun onNewGooglePlaceSelected(locationInfo: LocationInfo)

        fun onNavMenuItemClick(menuItem: MenuItem)

        fun onAddToFavButtonClick()
    }
}