package com.anadea.svv.kotlin.sunnyday.features.customize_dialog

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import kotlinx.android.synthetic.main.location_list_item.view.*
import java.lang.ref.WeakReference
import java.util.ArrayList
import java.util.HashSet

class LocationsDialogContentAdapter(context: Context,
                                    private var itemClickListenerReference: WeakReference<LocationListItemViewHolder.ItemClickListener>?) : RecyclerView.Adapter<LocationListItemViewHolder>() {

    private var locationInfoList: List<LocationInfo> = ArrayList()
    var selectedLocations: MutableSet<LocationInfo> = HashSet()
        private set

    private val bkgColorNormal: Int = ContextCompat.getColor(context, R.color.white)
    private val bkgColorSelected: Int = ContextCompat.getColor(context, R.color.white_smoke)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): LocationListItemViewHolder {
        val itemView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.location_list_item, parent, false)
        return LocationListItemViewHolder(itemView, itemClickListenerReference!!)
    }

    override fun onBindViewHolder(holder: LocationListItemViewHolder?, position: Int) {
        holder?.let {
            with(locationInfoList[position]) {
                val selected: Boolean = selectedLocations.contains(this)
                it.itemView.location_list_item_title_textView.text = cityName
                it.itemView.location_list_item_check_imageView
                        .setImageResource(if (selected) R.drawable.ic_delete_24dp
                        else R.drawable.ic_checkbox_unchecked_24dp)
                it.itemView.setBackgroundColor(if (selected) bkgColorSelected else bkgColorNormal)
            }
        }
    }

    override fun getItemCount(): Int = locationInfoList.size

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView?) {
        super.onDetachedFromRecyclerView(recyclerView)
        itemClickListenerReference = null
    }

    fun updateDataSet(locationInfoList: List<LocationInfo>) {
        this.locationInfoList = locationInfoList
        notifyDataSetChanged()
    }

    fun markItem(position: Int) {
        val locationInfo = locationInfoList[position]
        with(selectedLocations) {
            if (contains(locationInfo)) remove(locationInfo) else add(locationInfo)
        }
        notifyItemChanged(position)
    }

    fun clearSelection() {
        selectedLocations.clear()
        notifyDataSetChanged()
    }
}