package com.anadea.svv.kotlin.sunnyday.features.main

import android.app.Activity
import android.util.Log
import android.view.MenuItem
import com.anadea.svv.kotlin.sunnyday.App
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.DataManager
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoAdapter
import com.anadea.svv.kotlin.sunnyday.mvp.BasePresenter
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import io.reactivex.functions.Consumer
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

class MainPresenter : BasePresenter<MvpMainContract.View>(),
        MvpMainContract.Presenter {

    companion object {
        private const val TAG = "MainPresenter"
    }

    private enum class PresenterState {
        NO_DATA, LOCATION_RECEIVED, WEATHER_RECEIVED
    }

    @Singleton
    @Inject
    protected lateinit var dataManager: DataManager

    private var presenterState: PresenterState
    private var currentLocation: LocationInfo? = null   // Selected Location (GPS location at start)
        set(value) {
            field = value
            if (dataManager.internetIsAvailable)
                requestWeatherInfoUpdate()
            else
                getView()?.onError(dataManager.context.getString(R.string.err_no_internet))
        }

    private val isAddToFavEnabled: Boolean
        get() = dataManager.userGpsLocationInfo !== currentLocation
                && !dataManager.isLocationInFavorites(currentLocation!!.cityName)

    init {
        App.getAppComponent().inject(this)
        presenterState = PresenterState.NO_DATA
    }

    /**
     * Requesting stored user's Locations from SharedPreferences
     */
    private fun requestStoredUserLocations() {

        dataManager.requestAndPublishStoredUserLocations()
    }

    /**
     * Requesting stored WeatherInfo from SharedPreferences
     */
    private fun requestLastWeatherInfo() {

        compositeDisposable.add(dataManager.requestLastStoredWeatherInfo()
                .subscribe { weatherInfoAdapter ->
                    if (!weatherInfoAdapter.isEmpty)
                        getView()?.updateCurrentWeatherData(weatherInfoAdapter)
                })
    }

    /**
     * Requesting current user's GPS location
     */
    private fun requestGpsLocation() {

        dataManager.requestCurrentUserLocation()
                .doOnSubscribe { disposable ->
                    compositeDisposable.add(disposable)
                    getView()?.startProgress()
                }
                .subscribe { userGpsLocationInfo: LocationInfo, throwable: Throwable? ->
                    if (throwable != null) {
                        getView()?.let {
                            it.stopProgress()
                            it.onError(throwable.message!!)
                        }
                    } else {
                        if (userGpsLocationInfo.isEmpty) {
                            getView()?.let {
                                it.stopProgress()
                                it.onError(dataManager.context.getString(R.string.err_no_places_found))
                            }
                        } else {
                            // Update GPS Location info in Navigation Drawer header
                            getView()?.updateNavHeaderLocationData(userGpsLocationInfo)
                            // Applying current location
                            currentLocation = userGpsLocationInfo
                            presenterState = PresenterState.LOCATION_RECEIVED
                        }
                    }
                }
    }


    /**
     * Requesting Current weather info and Forecast info update for current location
     */
    private fun requestWeatherInfoUpdate() {
        currentLocation?.let {
            if (!it.isEmpty) {
                val (_, _, latitude, longitude) = it
                getView()?.startProgress()
                dataManager.requestAndPublishCurrentWeather(latitude!!, longitude!!)
                dataManager.requestAndPublishFiveDaysForecast(latitude, longitude)
            }
        }
    }

    /**
     * Subscription to user's locations info updates
     */
    private fun subscribeToUserLocationsUpdates() {

        compositeDisposable.add(dataManager
                .subscribeUserLocationsConsumer(Consumer { locationInfoList ->
                    getView()?.updateNavigationDrawerMenu(locationInfoList.sorted())
                }))
    }

    /**
     * Subscription to current weather info updates
     */
    private fun subscribeToCurrentWeatherInfoUpdates() {

        compositeDisposable.add(dataManager
                .subscribeCurrentWeatherConsumer(Consumer { weatherInfo ->
                    getView()?.stopProgress()
                    weatherInfo?.let {
                        presenterState = PresenterState.WEATHER_RECEIVED
                        val weatherInfoAdapter = WeatherInfoAdapter(currentLocation?.cityName, it)
                        getView()?.let {
                            it.updateCurrentWeatherData(weatherInfoAdapter)
                            it.setAddToFavButtonVisible(isAddToFavEnabled)

                            if (dataManager.userGpsLocationInfo === currentLocation)
                                dataManager.saveWeatherInfo(weatherInfoAdapter)
                        }
                    }
                }, Consumer { throwable ->
                    getView()?.let {
                        it.stopProgress()
                        it.onError(dataManager.parseErrorMessage(throwable).message)
                    }
                }))
    }

    /**
     * Subscription to forecast info updates
     */
    private fun subscribeToForecastUpdates() {

        compositeDisposable.add(dataManager
                .subscribeForecastPage(Consumer { forecastMap ->
                    getView()?.updateForecastDates(forecastMap.map { it.key })
                }, Consumer { throwable ->
                    getView()?.onError(dataManager.parseErrorMessage(throwable).message)
                }))
    }

    override fun attachView(mvpView: MvpMainContract.View) {
        super.attachView(mvpView)

        subscribeToUserLocationsUpdates()
        subscribeToCurrentWeatherInfoUpdates()
        subscribeToForecastUpdates()

        @Suppress("NON_EXHAUSTIVE_WHEN")
        when (presenterState) {
            PresenterState.NO_DATA -> {
                requestLastWeatherInfo()
                requestStoredUserLocations()
                getView()?.checkLocationPermissions()
            }
            PresenterState.LOCATION_RECEIVED -> requestWeatherInfoUpdate()
        }
    }

    override fun onLocationPermissionsGranted() {

        if (dataManager.internetIsAvailable)
            requestGpsLocation()
        else {
            getView()?.let {
                it.onError(dataManager.context.getString(R.string.err_no_internet))
                it.stopProgress()
            }
        }
    }

    override fun onRefreshRequested() {

        when (presenterState) {
            PresenterState.LOCATION_RECEIVED, PresenterState.WEATHER_RECEIVED -> requestWeatherInfoUpdate()
            else -> {
                requestStoredUserLocations()
                getView()?.checkLocationPermissions()
            }
        }
    }

    override fun onSearchButtonClick(activity: Activity, requestCode: Int) {

        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(activity)
            activity.startActivityForResult(intent, requestCode)
        } catch (e: GooglePlayServicesRepairableException) {
            getView()?.onError(String.format(Locale.getDefault(), "%s: %s",
                    dataManager.context.getString(R.string.err_google_services_error), e.message))
        } catch (e: GooglePlayServicesNotAvailableException) {
            getView()?.onError(String.format(Locale.getDefault(), "%s: %s",
                    dataManager.context.getString(R.string.err_google_services_na), e.message))
        }
    }

    override fun onNewGooglePlaceSelected(locationInfo: LocationInfo) {
        currentLocation = locationInfo
    }

    override fun onNavMenuItemClick(menuItem: MenuItem) {
        when (menuItem.itemId) {
            MainActivity.MENU_ITEM_ID_AUTODETECT -> getView()?.checkLocationPermissions()
            MainActivity.MENU_ITEM_ID_CUSTOMIZE -> getView()?.showLocationsCustomizeDialog()
            else -> dataManager.getStoredLocationInfoByName(menuItem.title.toString())?.let { currentLocation = it }
        }
    }

    override fun onAddToFavButtonClick() {
        currentLocation?.let {
            dataManager.addLocationToFavorites(it)
                    .subscribe { getView()?.setAddToFavButtonVisible(isAddToFavEnabled) }
        }
    }

}