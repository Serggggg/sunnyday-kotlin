package com.anadea.svv.kotlin.sunnyday.data.model.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Description (
        @SerializedName("description") @Expose val description: String? = null,
        @SerializedName("icon") @Expose val icon: String? = null)