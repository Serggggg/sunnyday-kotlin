package com.anadea.svv.kotlin.sunnyday.data.remote

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.util.*

class OpenWeatherInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        var request: Request = chain.request()
        val originalUrl = request.url()

        val newUrl: HttpUrl = originalUrl.newBuilder()
                .addQueryParameter("units", "metric")
                .addQueryParameter("appid", "a60cc944cdf20c4aa04bf53c75a35a66")
                .addQueryParameter("lang", Locale.getDefault().language)
                .build()

        request = request.newBuilder().url(newUrl).build()

        return chain.proceed(request)
    }
}