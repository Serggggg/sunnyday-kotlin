package com.anadea.svv.kotlin.sunnyday.features.customize_dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.mvp.BaseDialogFragment
import kotlinx.android.synthetic.main.custom_dialog_header.view.*
import kotlinx.android.synthetic.main.fragment_dialog_locations_customize_content.view.*
import java.lang.ref.WeakReference

class LocationsCustomizeDialog : BaseDialogFragment<MvpCustomizeDialogContract.View, MvpCustomizeDialogContract.Presenter>(),
        MvpCustomizeDialogContract.View,
        LocationListItemViewHolder.ItemClickListener {

    private var contentAdapter: LocationsDialogContentAdapter? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val rootView = LayoutInflater.from(activity).inflate(R.layout.fragment_dialog_locations_customize, null)

        contentAdapter = LocationsDialogContentAdapter(context, WeakReference(this))
        with(rootView) {
            custom_dialog_title_textView.setText(R.string.dialog_title_customize_locations)

            with(locations_dialog_recyclerView) {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = contentAdapter
            }

            custom_dialog_close_button.setOnClickListener { getPresenter()?.onCloseButtonClick() }
            locations_dialog_save_button.setOnClickListener { getPresenter()?.onRemoveButtonClick() }
            locations_dialog_clean_button.setOnClickListener { getPresenter()?.onClearButtonClick() }
        }

        return AlertDialog.Builder(activity).setView(rootView).create()
    }

    override fun createPresenter(): MvpCustomizeDialogContract.Presenter =
            LocationsCustomizePresenter()

    override fun onListItemClick(view: View, position: Int) {
        contentAdapter?.markItem(position)
    }

    override fun updateLocationsListData(locationInfoList: List<LocationInfo>) {
        contentAdapter?.updateDataSet(locationInfoList)
    }

    override fun clearSelection() {
        contentAdapter?.clearSelection()
    }

    override fun getSelectedLocations(): MutableSet<LocationInfo> =
            contentAdapter?.selectedLocations!!

    override fun closeDialog() = dismiss()
}