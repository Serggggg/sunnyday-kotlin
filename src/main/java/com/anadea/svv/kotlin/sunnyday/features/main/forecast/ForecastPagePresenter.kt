package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import android.util.Log
import com.anadea.svv.kotlin.sunnyday.App
import com.anadea.svv.kotlin.sunnyday.data.DataManager
import com.anadea.svv.kotlin.sunnyday.mvp.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject
import javax.inject.Singleton

class ForecastPagePresenter(private val pageNum: Int) : BasePresenter<MvpForecastContract.View>(),
        MvpForecastContract.Presenter {

    companion object {
        const val TAG = "ForecastPagePresenter"
    }

    @Singleton
    @Inject
    protected lateinit var dataManager: DataManager

    init {
        App.getAppComponent().inject(this)
    }

    override fun attachView(mvpView: MvpForecastContract.View) {
        super.attachView(mvpView)

        compositeDisposable.add(dataManager
                .subscribeForecastPage(Consumer { forecastMap ->
                    forecastMap?.let {
                        val keys = it.map { it.key }
                        getView()?.updateForecastData(it[keys[pageNum]]!!)
                    }
                }, Consumer { throwable ->
                    val errorMessage = dataManager.parseErrorMessage(throwable)
                    Log.e(TAG, errorMessage.message)
                }))
    }
}