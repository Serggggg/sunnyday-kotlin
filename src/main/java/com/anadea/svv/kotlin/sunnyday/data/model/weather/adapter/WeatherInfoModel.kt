package com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter

interface WeatherInfoModel {

    fun getLocationName(): String?

    fun getTemperature(): String

    fun getWeatherDate(): String

    fun getWeatherDateExt(): String

    fun getWeatherTime(): String

    fun getIconPath(): String

    fun getMaxTemperature(): String

    fun getMinTemperature(): String

    fun getHumidity(): String

    fun getWindSpeed(): String

    fun getPressure(): String

    fun getDescription(): String
}