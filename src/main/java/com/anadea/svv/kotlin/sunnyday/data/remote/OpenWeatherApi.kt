package com.anadea.svv.kotlin.sunnyday.data.remote

import com.anadea.svv.kotlin.sunnyday.data.model.forecast.ForecastInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {

    @GET("weather")
    fun getCurrentWeather(@Query("lat")latitude: Double, @Query("lon")longitude: Double) : Single<WeatherInfo>

    @GET("forecast")
    fun getFiveDaysForecast(@Query("lat")latitude: Double, @Query("lon")longitude: Double) : Single<ForecastInfo>

}