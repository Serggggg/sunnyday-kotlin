package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import com.anadea.svv.kotlin.sunnyday.mvp.IPresenter
import com.anadea.svv.kotlin.sunnyday.mvp.IView

interface MvpForecastContract {

    interface View : IView {

        fun updateForecastData(weatherInfoList: List<WeatherInfo>)
    }

    interface Presenter : IPresenter<View>
}