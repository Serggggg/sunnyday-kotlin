package com.anadea.svv.kotlin.sunnyday.mvp

import android.os.Bundle
import android.support.v4.app.DialogFragment

abstract class BaseDialogFragment<in V : IView, out P : IPresenter<V>> : DialogFragment() {

    private var mvpPresenter : P? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvpPresenter = createPresenter()
    }

    override fun onResume() {
        super.onResume()
        @Suppress("UNCHECKED_CAST")
        mvpPresenter?.attachView(this as V)
    }

    override fun onPause() {
        super.onPause()
        mvpPresenter?.detachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        mvpPresenter?.destroy()
        mvpPresenter = null
    }

    protected fun getPresenter() : P? = mvpPresenter

    abstract protected fun createPresenter() : P
}