package com.anadea.svv.kotlin.sunnyday.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<in V : IView, out P : IPresenter<V>> : AppCompatActivity() {

    private var mvpPresenter: P? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        @Suppress("UNCHECKED_CAST")
        mvpPresenter = if (lastCustomNonConfigurationInstance == null)
            createRetainedComponent()
        else
            lastCustomNonConfigurationInstance as P
    }

    override fun onStart() {
        super.onStart()
        @Suppress("UNCHECKED_CAST")
        mvpPresenter?.attachView(this as V)
    }

    override fun onStop() {
        super.onStop()
        mvpPresenter?.detachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isChangingConfigurations)
            mvpPresenter?.destroy()
        mvpPresenter = null
    }

    override fun onRetainCustomNonConfigurationInstance(): Any? = mvpPresenter

    protected fun getPresenter() : P? = mvpPresenter

    abstract protected fun createRetainedComponent() : P
}