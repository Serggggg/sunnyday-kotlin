package com.anadea.svv.kotlin.sunnyday.data.model.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WeatherInfo (
        @SerializedName("weather") @Expose val descriptionList : List<Description>,
        @SerializedName("main") @Expose val common : Common,
        @SerializedName("visibility") @Expose val visibility : Int? = null,
        @SerializedName("wind") @Expose val wind : Wind,
        @SerializedName("dt") @Expose val timestamp : Long,
        @SerializedName("sys") @Expose val solar : Solar)