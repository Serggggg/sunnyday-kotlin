package com.anadea.svv.kotlin.sunnyday.features.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoModel
import com.anadea.svv.kotlin.sunnyday.features.customize_dialog.LocationsCustomizeDialog
import com.anadea.svv.kotlin.sunnyday.features.main.forecast.ForecastPagerAdapter
import com.anadea.svv.kotlin.sunnyday.mvp.BaseActivity
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.forecast_info_layout.*
import kotlinx.android.synthetic.main.main_toolbar.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.weather_content_layout.*
import kotlinx.android.synthetic.main.weather_info_layout.*
import java.util.*


class MainActivity : BaseActivity<MvpMainContract.View, MvpMainContract.Presenter>(),
        MvpMainContract.View,
        MenuItem.OnMenuItemClickListener {

    companion object {
        private const val TAG = "MainActivity"
        private const val PERMISSION_REQUEST_LOCATION = 76
        private const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 986

        const val MENU_ITEM_ID_AUTODETECT = 65
        const val MENU_ITEM_ID_CUSTOMIZE = 730
    }

    private var pagerAdapter: ForecastPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with (swipe_refresh) {
            setOnRefreshListener { getPresenter()?.onRefreshRequested() }
            setColorSchemeResources(android.R.color.holo_purple, android.R.color.holo_green_light, android.R.color.holo_blue_light)
        }

        forecast_tabs?.setupWithViewPager(forecast_viewPager)
        pagerAdapter = ForecastPagerAdapter(supportFragmentManager)
        forecast_viewPager.adapter = pagerAdapter

        drawer_menu_button.setOnClickListener { main_drawer_layout.openDrawer(Gravity.START, true) }
        toolbar_search_button.setOnClickListener { getPresenter()?.onSearchButtonClick(this, PLACE_AUTOCOMPLETE_REQUEST_CODE) }
        add_to_favorites_fab.setOnClickListener { getPresenter()?.onAddToFavButtonClick() }
    }

    override fun createRetainedComponent(): MvpMainContract.Presenter = MainPresenter()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = PlaceAutocomplete.getPlace(this, data)
                    val locationInfo = LocationInfo(place.name.toString(), null,
                            place.latLng.latitude, place.latLng.longitude)
                    getPresenter()?.onNewGooglePlaceSelected(locationInfo)
                }
                PlaceAutocomplete.RESULT_ERROR -> {
                    val status = PlaceAutocomplete.getStatus(this, data)
                    Log.e(TAG, status.statusMessage)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (grantResults.any { it != PackageManager.PERMISSION_GRANTED })
                return
            getPresenter()?.onLocationPermissionsGranted()
        }
    }

    override fun onError(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

    override fun startProgress() {
        swipe_refresh.isRefreshing = true
    }

    override fun stopProgress() {
        swipe_refresh.isRefreshing = false
    }

    override fun checkLocationPermissions() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_LOCATION)
            else
                getPresenter()?.onLocationPermissionsGranted()
        }
        else {
            getPresenter()?.onLocationPermissionsGranted()
        }
    }

    override fun updateNavigationDrawerMenu(userLocationsList: List<LocationInfo>) {

        var order = 0
        with (main_nav_view.menu) {

            clear()
            add(0, MENU_ITEM_ID_AUTODETECT, order++, R.string.menu_item_label_autodetect)
                    .setIcon(R.drawable.ic_my_location_black_24dp)
                    .setOnMenuItemClickListener(this@MainActivity)

            userLocationsList.forEach {
                add(0, it.cityName.hashCode(), order++, it.cityName)
                        .setIcon(R.drawable.ic_place_black_24dp)
                        .setOnMenuItemClickListener(this@MainActivity)
            }

            add(1, MENU_ITEM_ID_CUSTOMIZE, order, R.string.menu_item_label_customize)
                    .setOnMenuItemClickListener(this@MainActivity)
        }
    }

    override fun updateNavHeaderLocationData(locationInfo: LocationInfo) {
        nav_header_city_textView.text = locationInfo.cityName
        nav_header_country_textView.text = locationInfo.countryName
        nav_header_latlong_textView.text = locationInfo.latLong
    }

    override fun updateCurrentWeatherData(weatherInfoModel: WeatherInfoModel) {

        val locale = Locale.getDefault()
        arrayOf(location_imageView, maxTemp_imageView, minTemp_imageView)
                .forEach { it.visibility = View.VISIBLE }
        Picasso.with(this)
                .load(weatherInfoModel.getIconPath())
                .into(weatherIcon_imageView)
        city_textView.text = weatherInfoModel.getLocationName()
        date_textView.text = weatherInfoModel.getWeatherDateExt()
        temp_textView.text = weatherInfoModel.getTemperature()
        maxTemp_textView.text = weatherInfoModel.getMaxTemperature()
        minTemp_textView.text = weatherInfoModel.getMinTemperature()
        humidity_textView.text = String.format(locale, "%s: %s",
                getString(R.string.str_label_humidity), weatherInfoModel.getHumidity())
        wind_textView.text = String.format(locale, "%s: %s %s", getString(R.string.str_label_wind),
                weatherInfoModel.getWindSpeed(), getString(R.string.str_label_m_sec))
        description_textView.text = weatherInfoModel.getDescription()
    }

    override fun setAddToFavButtonVisible(enableAddToFav: Boolean) {
        add_to_favorites_fab.apply { if (enableAddToFav) this.show() else this.hide() }
    }

    override fun updateForecastDates(forecastDates: List<String>) {
        pagerAdapter?.updatePageTitles(forecastDates)
    }

    override fun showLocationsCustomizeDialog() =
            LocationsCustomizeDialog().show(supportFragmentManager, "Locations customize")

    override fun onMenuItemClick(item: MenuItem): Boolean {
        getPresenter()?.onNavMenuItemClick(item)
        if (main_drawer_layout.isDrawerOpen(Gravity.START))
            main_drawer_layout.closeDrawer(Gravity.START)
        return true
    }

}