package com.anadea.svv.kotlin.sunnyday.di.components

import com.anadea.svv.kotlin.sunnyday.data.DataManager
import com.anadea.svv.kotlin.sunnyday.di.modules.AppModule
import com.anadea.svv.kotlin.sunnyday.di.modules.DataModule
import com.anadea.svv.kotlin.sunnyday.features.customize_dialog.LocationsCustomizePresenter
import com.anadea.svv.kotlin.sunnyday.features.main.MainPresenter
import com.anadea.svv.kotlin.sunnyday.features.main.forecast.ForecastPagePresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, DataModule::class))
interface AppComponent {

    fun inject(dataManager: DataManager)

    fun inject(presenter: MainPresenter)

    fun inject(presenter: ForecastPagePresenter)

    fun inject(presenter: LocationsCustomizePresenter)
}