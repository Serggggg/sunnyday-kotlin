package com.anadea.svv.kotlin.sunnyday.data.model.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Solar (
        @SerializedName("sunrise") @Expose val sunrise: Long? = null,
        @SerializedName("sunset") @Expose val sunset: Long? = null)