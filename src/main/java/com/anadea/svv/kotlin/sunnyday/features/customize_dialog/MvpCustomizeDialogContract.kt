package com.anadea.svv.kotlin.sunnyday.features.customize_dialog

import com.anadea.svv.kotlin.sunnyday.data.model.location.LocationInfo
import com.anadea.svv.kotlin.sunnyday.mvp.IPresenter
import com.anadea.svv.kotlin.sunnyday.mvp.IView

interface MvpCustomizeDialogContract {

    interface View : IView {

        fun updateLocationsListData(locationInfoList: List<LocationInfo>)

        fun clearSelection()

        fun getSelectedLocations(): MutableSet<LocationInfo>

        fun closeDialog()
    }

    interface Presenter : IPresenter<View> {

        fun onCloseButtonClick()

        fun onRemoveButtonClick()

        fun onClearButtonClick()
    }
}