package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class ForecastPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private var forecastDates: List<String> = ArrayList()

    fun updatePageTitles(pageTitles: List<String>) {
        forecastDates = pageTitles
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment = ForecastPageFragment.newInstance(position)

    override fun getCount(): Int = forecastDates.size

    override fun getPageTitle(position: Int): CharSequence = forecastDates[position]
}