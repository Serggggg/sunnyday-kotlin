package com.anadea.svv.kotlin.sunnyday;

import android.app.Application;

import com.anadea.svv.kotlin.sunnyday.di.components.AppComponent;
import com.anadea.svv.kotlin.sunnyday.di.components.DaggerAppComponent;
import com.anadea.svv.kotlin.sunnyday.di.modules.AppModule;

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
