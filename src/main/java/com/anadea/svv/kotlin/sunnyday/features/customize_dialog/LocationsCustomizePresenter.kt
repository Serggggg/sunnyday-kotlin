package com.anadea.svv.kotlin.sunnyday.features.customize_dialog

import com.anadea.svv.kotlin.sunnyday.App
import com.anadea.svv.kotlin.sunnyday.data.DataManager
import com.anadea.svv.kotlin.sunnyday.mvp.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject
import javax.inject.Singleton

class LocationsCustomizePresenter : BasePresenter<MvpCustomizeDialogContract.View>(),
        MvpCustomizeDialogContract.Presenter {

    @Singleton
    @Inject
    protected lateinit var dataManager: DataManager

    init {
        App.getAppComponent().inject(this)
    }

    /**
     * Subscription to user's locations info updates
     */
    private fun subscribeToUserLocationsUpdates() {

        compositeDisposable.add(dataManager
                .subscribeUserLocationsConsumer(Consumer { locationInfoList ->
                    getView()?.updateLocationsListData(locationInfoList)
                }))
    }

    override fun attachView(mvpView: MvpCustomizeDialogContract.View) {
        super.attachView(mvpView)
        subscribeToUserLocationsUpdates()
    }

    override fun onCloseButtonClick() {
        getView()?.closeDialog()
    }

    override fun onRemoveButtonClick() {
        val locationsToRemoveList = getView()?.getSelectedLocations()?.map { it }
        locationsToRemoveList?.let {
            if (it.isEmpty())
                getView()?.closeDialog()
            else
                dataManager.removeLocationsFromFavorites(locationsToRemoveList)
                        .subscribe { getView()?.closeDialog() }
        }
    }

    override fun onClearButtonClick() {
        getView()?.clearSelection()
    }
}