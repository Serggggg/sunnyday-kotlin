package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoAdapter
import java.util.ArrayList

class ForecastContentAdapter : RecyclerView.Adapter<ForecastItemViewHolder>() {

    private var weatherInfoAdapterList: List<WeatherInfoAdapter> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastItemViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.forecast_list_item, parent, false)
        return ForecastItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ForecastItemViewHolder?, position: Int) {
        holder?.bindWeatherInfoData(weatherInfoAdapterList[position])
    }

    override fun getItemCount(): Int = weatherInfoAdapterList.size

    fun updateData(weatherInfoList: List<WeatherInfoAdapter>) {
        this.weatherInfoAdapterList = weatherInfoList
        notifyDataSetChanged()
    }
}