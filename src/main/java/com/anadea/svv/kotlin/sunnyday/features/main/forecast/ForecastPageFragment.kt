package com.anadea.svv.kotlin.sunnyday.features.main.forecast

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anadea.svv.kotlin.sunnyday.R
import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo
import com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter.WeatherInfoAdapter
import com.anadea.svv.kotlin.sunnyday.mvp.BaseFragment
import kotlinx.android.synthetic.main.fragment_forecast_page.*

class ForecastPageFragment : BaseFragment<MvpForecastContract.View, MvpForecastContract.Presenter>(),
        MvpForecastContract.View {

    companion object {

        private const val TAG = "ForecastPageFragment"

        private const val KEY_PAGE_NUM = "page_num"

        fun newInstance(pageNum: Int): ForecastPageFragment {
            val args = Bundle()
            args.putInt(KEY_PAGE_NUM, pageNum)
            val fragment = ForecastPageFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var contentAdapter: ForecastContentAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_forecast_page, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contentAdapter = ForecastContentAdapter()

        with(forecast_recyclerView) {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            adapter = contentAdapter
        }
    }

    override fun createPresenter(): MvpForecastContract.Presenter =
            ForecastPagePresenter(arguments.getInt(KEY_PAGE_NUM))

    override fun updateForecastData(weatherInfoList: List<WeatherInfo>) {
        contentAdapter?.updateData(WeatherInfoAdapter.transformWeatherInfoList("", weatherInfoList))
    }
}