package com.anadea.svv.kotlin.sunnyday.data.model.location;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LocationInfoTest {

    @Test
    public void locationInfoSortTest() {

        String[] cityNames = {"Dnipro", "Kyiv", "Lviv"};

        List<LocationInfo> locationInfoList = new ArrayList<>();
        locationInfoList.add(new LocationInfo(cityNames[2], null, 0.0, 0.0));
        locationInfoList.add(new LocationInfo(cityNames[1], null, 0.0, 0.0));
        locationInfoList.add(new LocationInfo(cityNames[0], null, 0.0, 0.0));
        Collections.sort(locationInfoList);

        for (int i = 0; i < 3; i++)
            assertEquals(locationInfoList.get(i).getCityName(), cityNames[i]);

        LocationInfo locationInfo = new LocationInfo("", null, null, null);
        assertTrue(locationInfo.isEmpty());
    }
}
