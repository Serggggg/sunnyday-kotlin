package com.anadea.svv.kotlin.sunnyday.data.model.weather.adapter;

import com.anadea.svv.kotlin.sunnyday.data.model.weather.WeatherInfo;
import com.google.gson.Gson;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WeatherInfoAdapterTest {

    private static final String WEATHER_INFO_JSON = "{\"coord\":{\"lon\":35.05,\"lat\":48.46},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"base\":\"stations\",\"main\":{\"temp\":25.58,\"pressure\":1010,\"humidity\":38,\"temp_min\":25,\"temp_max\":26},\"visibility\":10000,\"wind\":{\"speed\":9,\"deg\":90},\"clouds\":{\"all\":0},\"dt\":1506061800,\"sys\":{\"type\":1,\"id\":7351,\"message\":0.0137,\"country\":\"UA\",\"sunrise\":1506050807,\"sunset\":1506094627},\"id\":700022,\"name\":\"Nizhnedneprovsk\",\"cod\":200}";
    private static final String LOCATION_NAME = "Test Location";

    @Test
    public void adapterTest() {

        WeatherInfoAdapter adapter = new WeatherInfoAdapter();
        assertTrue(adapter.isEmpty());

        adapter = new WeatherInfoAdapter(LOCATION_NAME, null);
        assertFalse(adapter.isEmpty());

        WeatherInfo weatherInfo = new Gson().fromJson(WEATHER_INFO_JSON, WeatherInfo.class);
        adapter = new WeatherInfoAdapter(null, weatherInfo);
        assertFalse(adapter.isEmpty());

        adapter = new WeatherInfoAdapter(LOCATION_NAME, weatherInfo);
        assertFalse(adapter.isEmpty());

        assertEquals(adapter.getLocationName(), LOCATION_NAME);

        assertEquals(adapter.getTemperature(), "+25");

        long timestamp = weatherInfo.getTimestamp() * 1000;
        Locale locale = Locale.getDefault();
        Date date = new Date(timestamp);
        assertEquals(adapter.getWeatherDate(), new SimpleDateFormat("dd MMM yyyy", locale).format(date));
        assertEquals(adapter.getWeatherDateExt(), new SimpleDateFormat("EEEE, d MMM yyyy", locale).format(date));
        assertEquals(adapter.getWeatherTime(), new SimpleDateFormat("HH:mm", locale).format(date));

        assertEquals(adapter.getIconPath(), "http://openweathermap.org/img/w/01d.png");

        assertEquals(adapter.getMaxTemperature(), "26");
        assertEquals(adapter.getMinTemperature(), "25");
        assertEquals(adapter.getHumidity(), "38%");
        assertEquals(adapter.getWindSpeed(), "9");
        assertEquals(adapter.getPressure(), "1010");
        assertEquals(adapter.getDescription(), "Clear sky");
    }

    @Test
    public void modelTest() {

        WeatherInfo weatherInfo = new Gson().fromJson(WEATHER_INFO_JSON, WeatherInfo.class);
        WeatherInfoModel weatherInfoModel = new WeatherInfoAdapter(LOCATION_NAME, weatherInfo);

        assertEquals(weatherInfoModel.getLocationName(), LOCATION_NAME);

        assertEquals(weatherInfoModel.getTemperature(), "+25");

        long timestamp = weatherInfo.getTimestamp() * 1000;
        Locale locale = Locale.getDefault();
        Date date = new Date(timestamp);
        assertEquals(weatherInfoModel.getWeatherDate(), new SimpleDateFormat("dd MMM yyyy", locale).format(date));
        assertEquals(weatherInfoModel.getWeatherDateExt(), new SimpleDateFormat("EEEE, d MMM yyyy", locale).format(date));
        assertEquals(weatherInfoModel.getWeatherTime(), new SimpleDateFormat("HH:mm", locale).format(date));

        assertEquals(weatherInfoModel.getIconPath(), "http://openweathermap.org/img/w/01d.png");

        assertEquals(weatherInfoModel.getMaxTemperature(), "26");
        assertEquals(weatherInfoModel.getMinTemperature(), "25");
        assertEquals(weatherInfoModel.getHumidity(), "38%");
        assertEquals(weatherInfoModel.getWindSpeed(), "9");
        assertEquals(weatherInfoModel.getPressure(), "1010");
        assertEquals(weatherInfoModel.getDescription(), "Clear sky");
    }

}
